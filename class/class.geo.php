<?php
function geocode($address){
    $address = urlencode($address);
    $googlemap = "http://maps.google.com/maps/api/geocode/json?sensor=false&address={$address}";
    $get_data = file_get_contents($googlemap);
    $jslink = json_decode($get_data, true);

    $latitude = $jslink ['results']['0']['geometry']['location']['lat'];
    $longitude = $jslink['results']['0']['geometry']['location']['lng'];
    $town = $jslink['results']['0']['formatted_address'];
    if ($latitude && $longitude && $town) {
        $dump_arry = array();

        array_push(
            $dump_arry,
            $latitude,
            $longitude,
            $town

        );

        return $dump_arry;

    }else{
        return "ERROR";

    }
}

?>