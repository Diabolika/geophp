<?php
require 'lib/forecast.io.php';
?>
<?php

$latitude = '52.4308';
$longitude = '13.2588';
$api_key = '6d38c544b858c9067d2d0a99e1268ea7';
$units = 'si';  // Can be set to 'us', 'si', 'ca', 'uk' or 'auto' (see forecast.io API); default is auto
$lang = 'fr'; // Can be set to 'en', 'de', 'pl', 'es', 'fr', 'it', 'tet' or 'x-pig-latin' (see forecast.io API); default is 'en'
$forecast = new ForecastIO($api_key);
$condition = $forecast->getCurrentConditions($latitude, $longitude, $units, $lang);
$conditions_today = $forecast->getForecastWeek($latitude, $longitude, $units, $lang);
?>